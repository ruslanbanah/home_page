var init = (obj) => {
  var scriptTag = "<script> let script = document.createElement('script'); script.src = '/build/admin/scripts/editor.js'; script.type = 'text/javascript'; document.getElementsByTagName('head')[0].appendChild(script); <";
  scriptTag += "/script>";
  $("#iframe").contents().find("body").append(scriptTag);

  setTimeout(() => {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    $(".iframe.preloader").removeClass('preloader');
  }, 2000)
}

var update_field = function (field, value) {
  $(`.storage_field[data-element="${field}"]`).val(value)
}