var scripts = [
  "/build/admin/scripts/froala_editor.min.js",
  "/build/admin/scripts/align.min.js",
  "/build/admin/scripts/char_counter.min.js",
  "/build/admin/scripts/code_view.min.js",
  "/build/admin/scripts/code_beautifier.min.js",
  "/build/admin/scripts/colors.min.js",
  "/build/admin/scripts/entities.min.js",
  "/build/admin/scripts/font_family.min.js",
  "/build/admin/scripts/font_size.min.js",
  "/build/admin/scripts/inline_style.min.js",
  "/build/admin/scripts/line_breaker.min.js",
  "/build/admin/scripts/link.min.js",
  "/build/admin/scripts/lists.min.js",
  "/build/admin/scripts/paragraph_format.min.js",
  "/build/admin/scripts/paragraph_style.min.js",
  "/build/admin/scripts/quick_insert.min.js",
  "/build/admin/scripts/quote.min.js",
  "/build/admin/scripts/url.min.js",
  "/build/admin/scripts/video.min.js"
]
var scripts_index = 0

var styles = [
  "/build/admin/styles/froala/froala_editor.css",
  "/build/admin/styles/froala/froala_style.css",
  "/build/admin/styles/froala/code_view.css",
  "/build/admin/styles/froala/colors.css",
  "/build/admin/styles/froala/line_breaker.css",
  "/build/admin/styles/froala/quick_insert.css",
  "/build/admin/styles/froala/video.css",
  "/build/admin/styles/font-awesome.css",
  "/build/admin/styles/editor.css"
]

var inject_scripts = (index = 0) => {
  let script = document.createElement("script")
  script.src = scripts[index]
  script.type = 'text/javascript'
  script.onload = () => {
    if (scripts[index + 1]) {
      scripts_index++
      inject_scripts(scripts_index)
    } else {
      run_text_editors()
    }
  }

  document.getElementsByTagName("head")[0].appendChild(script)
}

var inject_styles = () => {
  styles.forEach((style) => {
    window.page.append(`<link rel="stylesheet" href="${style}" type="text/css" />`)
  })
}

var init = () => {
  let script = document.createElement("script")
  script.src = '/build/scripts/jquery.js'
  script.type = 'text/javascript'
  document.getElementsByTagName("head")[0].appendChild(script)

  setTimeout(() => {
    window.page = $('body')

    inject_scripts(scripts_index)
    inject_styles()

    $("header").hide()

    $('.services__footer-btn').click(function () {
      console.log('services__footer-btn')
    })

  }, 1000)
}

var run_text_editors = () => {
  let default_config = {
    toolbarInline: true,
    enter: $.FroalaEditor.ENTER_BR,
    charCounterCount: false,
    toolbarVisibleWithoutSelection: true,
    initOnClick: true,
    toolbarButtons: [
      'bold', 'italic', 'underline', 'strikeThrough', 'color', '-', 'paragraphFormat', 'align', 'indent', 'outdent', '-', 'insertLink', 'insertVideo', 'html', 'undo', 'redo']
  }

  $('*[data-editable="text_inline"]').froalaEditor(
      $.extend(default_config, {})
  )

  $('*[data-editable="text_popup"]').unbind('click').froalaEditor(
      $.extend(default_config, {editInPopup: true, initOnClick: false})
  )

  $('*[data-editable="text_inline"]').prepend('<div class="editor_tooltip">text inline</div>')
  $('*[data-editable="text_popup"]').prepend('<div class="editor_tooltip">text popup</div>')

  $('*[data-editable="text_popup"]').prepend(
      '<div class="editor_buttons">' +
      '<div class="button edit"><i class="fa fa-pencil"></i></div>' +
      '</div>'
  )

  $('*[data-editable]').on('froalaEditor.contentChanged', function (e, editor) {
    console.log(editor.$el.parent().parent().data('element'))
    console.log(editor.html.get())
    parent.update_field(editor.$el.parent().parent().data('element'), editor.html.get())
  });

  $.ajax()
}

init()
