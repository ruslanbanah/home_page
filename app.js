var express = require('express');
var path = require('path');
var app = express();

/**
 * Use public folder.
 */
app.use(express.static('public'));

/**
 * Get index page.
 */
app.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname + '/views/index.html'));
});

/**
 * Start listen port 8080.
 */
app.listen(8080, function () {
  console.log('http://localhost:8080');
});
